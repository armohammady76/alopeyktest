<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Store extends Model
{

	use HasFactory;

	protected $fillable=["name", "lat", "lng", "radius", "seller_id"];

	public function Seller ()
	{
		$this->belongsTo(Seller::class);
	}

	public function Products ()
	{
		return $this->hasMany(Product::class);
	}

	public static function GetNearby ($lat, $lng)
	{
		$stores=DB::select(DB::raw("SELECT x.id
								FROM
    							(
       									 SELECT radius,id, SQRT(POW((lat-$lat),2)+POW((lng-$lng),2)) as dis
        								FROM stores
   								 ) AS x
								where x.dis <= x.radius ORDER BY x.dis"));
		$storesId=[];
		foreach($stores as $store)
		{
			$storesId[]=$store->id;
		}
		return self::whereIn("id",$storesId)->get();
	}

	public function InRang ($lat, $lng)
	{
		$dis=sqrt(pow($this->lat-$lat,2)+pow($this->lng-$lng,2));

		return $dis<=$this->radius;

	}
}
