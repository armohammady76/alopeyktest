<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable=["name","price","store_id"];

	public function Store ()
	{
		return $this->belongsTo(Store::class);
    }
}
