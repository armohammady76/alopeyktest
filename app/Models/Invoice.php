<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable=["total_price","store_id","customer_id","status"];

	public function Products ()
	{
		return $this->hasMany(InvoiceProduct::class);
    }

	public function Payment ()
	{
		return $this->hasOne(Payment::class);
	}
}
