<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceProduct extends Model
{

    use HasFactory,SoftDeletes;

    protected $fillable=["name","price","invoice_id","number","product_id"];
}
