<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Customer extends User
{

	use HasFactory;

	public function Invoices ()
	{
		return $this->hasMany(Invoice::class, "customer_id", "id");
	}

	public function PendingInvoice ()
	{
		return $this->Invoices()->where("status", "pending");
	}

	public function Order (Product $product, $number)
	{
		$invoice=$this->PendingInvoice()->first();
		if(empty($invoice))
		{
			$invoice=Invoice::create([
				"total_price"=>$number*$product->price,
				"status"     =>"pending",
				"store_id"   =>$product->store_id,
				"customer_id"=>Auth::id(),
			]);

		}
		else
		{
			if($product->store_id != $invoice->store_id)
			{
				abort(422, "There is pending invoice from another store!");
			}
			$invoice->increment("total_price", $number*$product->price);
		}

		$invoiceProduct=new InvoiceProduct([
			"name"      =>$product->name,
			"price"     =>$product->price,
			"number"    =>$number,
			"product_id"=>$product->id,
		]);

		$invoice->Products()->save($invoiceProduct);

		return $invoice->with("products")->get();
	}


}
