<?php

namespace App\Policies;

use App\Models\Store;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SellerStorePolicy
{

	use HandlesAuthorization;

	public function before (User $user)
	{
		if($user->type=="admin")
		{
			return true;
		}
	}
	/**
	 * Determine whether the user can view any models.
	 *
	 * @param \App\Models\User $user
	 *
	 * @return mixed
	 */
	public function viewAny (User $user)
	{
		return true;
	}

	/**
	 * Determine whether the user can view the model.
	 *
	 * @param \App\Models\User  $user
	 * @param \App\Models\Store $store
	 *
	 * @return mixed
	 */
	public function view (User $user, Store $store)
	{
		return true;
	}

	/**
	 * Determine whether the user can create models.
	 *
	 * @param \App\Models\User $user
	 *
	 * @return mixed
	 */
	public function create (User $user)
	{
		return $user->type=="seller";
	}

	/**
	 * Determine whether the user can update the model.
	 *
	 * @param \App\Models\User  $user
	 * @param \App\Models\Store $store
	 *
	 * @return mixed
	 */
	public function update (User $user, Store $store)
	{
		dd($user->type);

		if($user->type == "seller")
		{
			return $user->id == $store->seller_id;
		}

		return false;
	}

	/**
	 * Determine whether the user can delete the model.
	 *
	 * @param \App\Models\User  $user
	 * @param \App\Models\Store $store
	 *
	 * @return mixed
	 */
	public function delete (User $user, Store $store)
	{
		if($user->type == "seller")
		{
			return $user->id == $store->seller_id;
		}

		return false;
	}

	/**
	 * Determine whether the user can restore the model.
	 *
	 * @param \App\Models\User  $user
	 * @param \App\Models\Store $store
	 *
	 * @return mixed
	 */
	public function restore (User $user, Store $store)
	{
		if($user->type == "seller")
		{
			return $user->id == $store->seller_id;
		}

		return false;
	}

	/**
	 * Determine whether the user can permanently delete the model.
	 *
	 * @param \App\Models\User  $user
	 * @param \App\Models\Store $store
	 *
	 * @return mixed
	 */
	public function forceDelete (User $user, Store $store)
	{
		return false;
	}
}
