<?php

namespace App\Providers;

use App\Models\Product;
use App\Models\Store;
use App\Policies\SellerProductPolicy;
use App\Policies\SellerStorePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{

	/**
	 * The policy mappings for the application.
	 *
	 * @var array
	 */
	protected $policies=[
		Store::class  =>SellerStorePolicy::class,
		Product::class=>SellerProductPolicy::class,
	];

	/**
	 * Register any authentication / authorization services.
	 *
	 * @return void
	 */
	public function boot ()
	{
		$this->registerPolicies();


		Passport::tokensCan([
			'admin'   =>"admin user",
			'seller'  =>"seller user",
			'customer'=>"customer user",
		]);


	}
}
