<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{

	public function Order (OrderRequest $request)
	{
		$validData=$request->validated();

		$product=Product::findOrFail($validData["product_id"]);
		/**@var Store $store
		*/
		$store=$product->Store()->first();

		if(!$store->InRang($validData["lat"],$validData["lng"]))
		{
			abort(422,"Store not in your range!");
		}

		$customer=new Customer(["id"=>Auth::id()]);

		$invoice=$customer->Order($product,$validData["number"]);

		return $this->successResponse($invoice);


	}
}
