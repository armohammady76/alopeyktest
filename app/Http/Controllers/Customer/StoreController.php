<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\LocationRequest;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StoreController extends Controller
{

	public function ShowAll ()
	{
		return $this->successResponse(Store::all());
	}

	public function Show ($store)
	{
		return $this->successResponse(Store::where("id", $store)->with("products")->get());
	}

	public function NearbyStores (LocationRequest $request)
	{
		$validData=$request;

		return $this->successResponse(Store::GetNearby($validData[ "lat" ],$validData[ "lng" ]));
	}
}
