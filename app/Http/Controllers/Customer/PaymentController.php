<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentRequest;
use App\Models\Invoice;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PaymentController extends Controller
{

	public function Pay (PaymentRequest $request)
	{
		$validData=$request->validated();
		/**@var Invoice $invoice
		*/
		$invoice=Invoice::findOrFail($validData["invoice_id"]);

		if($invoice->customer_id!=Auth::id())
		{
			abort(403);
		}

		$invoice->update(["status"=>"confirmed"]);

		$payment=new Payment([
			"customer_id"=>$invoice->coustomer_id,
			"status"=>"pending",
			"amount"=>$invoice->total_price,
			"transaction_id"=>Str::random(),
		]);

		$invoice->Payment()->save($payment);


		return $this->successResponse($invoice);
    }

	public function CallBack (Request $request)
	{
		$payment=Payment::where("transaction_id",$request->input("transaction_id"))->first();

		$invoice=$payment->Invoice()->first();

		$invoice->update(["status"=>"successful"]);

		return $this->successResponse($invoice);

	}
}
