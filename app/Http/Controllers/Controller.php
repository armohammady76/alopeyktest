<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{

	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	/**
	 * @param array $data
	 * @param int   $code
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function successResponse ($data=[], $code=200)
	{
		$response=[
			'status' =>'success',
			'payload'=>$data,
		];

		return response()->json($response, $code);
	}

	/**
	 * @param null  $message
	 * @param array $data
	 * @param int   $code
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function errorResponse ($message=null, $data=[], $code=500)
	{
		$response=[
			'status' =>'failure',
			'message'=>$message,
		];

		if($data)
		{
			$response[ 'payload' ]=$data;
		}

		return response()->json($response, $code);
	}

}
