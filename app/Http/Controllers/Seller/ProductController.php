<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Models\Store;
use App\Traits\CRUDActions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Console\Input\Input;

class ProductController extends Controller
{
    use CRUDActions;

    protected $entity=Product::class;

	public function __construct()
	{
		$this->authorizeResource(Product::class, 'product');
	}

	public function beforeSaveEntity ($data)
	{
		if(Store::where([["id",$data["id"]],["seller_id",Auth::id()]])->count()==0)
		{
			abort(403);
		}
	}
	public function index ()
	{
		$data=Store::where("seller_id",Auth::id())->select("id","name")->with("products")->get();
		return $this->successResponse($data);
	}


}
