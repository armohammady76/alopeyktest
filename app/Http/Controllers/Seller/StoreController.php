<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use App\Models\Seller;
use App\Models\Store;
use App\Traits\CRUDActions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StoreController extends Controller
{
    use CRUDActions;

    protected $entity=Store::class;

	public function __construct()
	{
		$this->authorizeResource(Store::class, 'store');
	}

	public function beforeSaveEntity (&$data)
	{
		$data["seller_id"]=Auth::id();
    }

	public function index ()
	{
		/**@var Seller $seller
		*/
		$seller=Seller::find(Auth::id());
		return $this->successResponse($seller->Stores()->get());
	}

}
