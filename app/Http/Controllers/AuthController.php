<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
	public function register(RegisterRequest $request)
	{
		$validatedData = $request->validated();

		$validatedData['password'] = bcrypt($request->input("password"));

		$user = User::create($validatedData);

		Auth::loginUsingId($user->id);

		$accessToken = $user->createToken('authToken',["customer"])->accessToken;

		return $this->successResponse([ 'user' => $user, 'access_token' => $accessToken]);
	}

	public function login(LoginRequest $request)
	{
		$loginData = $request->validated();

		if (!auth()->attempt($loginData)) {
			return $this->errorResponse( 'Invalid Credentials',[],422);
		}
		/** @var User $user*/
		$user=auth()->user();
		$accessToken = $user->createToken('authToken',[$user->type])->accessToken;

		return $this->successResponse(['user' => auth()->user(),"access_token"=>$accessToken]);

	}
}
