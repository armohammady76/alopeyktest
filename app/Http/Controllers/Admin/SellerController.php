<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SellerRequest;
use App\Models\User;
use App\Traits\CRUDActions;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SellerController extends Controller
{
    use CRUDActions;

    protected $entity=User::class;

	public function store (SellerRequest $request)
	{
		$validData=$request->validated();

		$password=Str::random(8);

		$validData["password"]=bcrypt($password);

		$validData["type"]="seller";

		$user=User::create($validData);

		return $this->successResponse(["user"=>$user,"password"=>$password]);
    }

}
