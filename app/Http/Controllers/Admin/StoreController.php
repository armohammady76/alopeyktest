<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRequest;
use App\Models\Seller;
use App\Models\Store;
use App\Models\User;
use App\Traits\CRUDActions;
use Illuminate\Http\Request;

class StoreController extends Controller
{

	use CRUDActions;

	protected $entity=Store::class;

}
