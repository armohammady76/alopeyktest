<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Traits\CRUDActions;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use CRUDActions;

    protected $entity=Product::class;
}
