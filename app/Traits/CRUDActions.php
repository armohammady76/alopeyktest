<?php


namespace App\Traits;



use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait CRUDActions
{
    protected function getEntityHumanizedName() {
        return Str::title(Str::snake(Str::afterLast($this->entity, "\\"), " "));
    }

    protected function getRequestClass() {
        $entity = Str::afterLast($this->entity, "\\");
        return "App\\Http\\Requests\\" . $entity . "Request";
    }

    protected function getUpdateRequestClass() {
        $entity = Str::afterLast($this->entity, "\\");
        $class = "App\\Http\\Requests\\" . $entity . "UpdateRequest";
        return class_exists($class) ? $class : $this->getRequestClass();
    }

    protected function afterUpdateEntity($item) {

    }

	protected function beforeSaveEntity(&$data)
	{

	}
	protected function beforeUpdateEntity(&$data)
	{

	}
    protected function afterSaveEntity($item) {

    }

    /**
     * @return JsonResponse
     */
    public function index() {
        $data = $this->entity::query()->get();
        return $this->successResponse($data);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show($id) {
        $entity = $this->entity::find($id);
        if (!$entity)
            throw new NotFoundHttpException($this->getEntityHumanizedName() . " not found.");

        return $this->successResponse($entity);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request) {
        $requestClass = $this->getRequestClass();
        $requestInstance = new $requestClass();
        $validator = Validator::make($request->all(), $requestInstance->rules());
        $data=$validator->validate();

        $this->beforeSaveEntity($data);

        $item = new  $this->entity($data);
        $item->save();

        $this->afterSaveEntity($item);


        return $this->successResponse($item);
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request, $id) {
        $requestClass = $this->getUpdateRequestClass();
        $requestInstance = new $requestClass();
        $validator = Validator::make($request->all(), $requestInstance->rules());
        $data=$validator->validate();

        $item = $this->entity::find($id);
        if (!$item)
            throw new NotFoundHttpException($this->getEntityHumanizedName() . " not found.");

	    $this->beforeSaveEntity($data);

	    $item->update($data);

        $this->afterUpdateEntity($item);

        return $this->successResponse($item);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id) {
        $item = $this->entity::find($id);
        if (!$item)
            throw new NotFoundHttpException($this->getEntityHumanizedName() . " not found.");

        $item->delete();
        return $this->successResponse();
    }
}
