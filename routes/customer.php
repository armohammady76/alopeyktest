<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Customer\StoreController;
use App\Http\Controllers\Customer\ProductController;
use App\Http\Controllers\Customer\PaymentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get("store", [StoreController::class, "ShowAll"]);
Route::get("store/{store}", [StoreController::class, "Show"]);
Route::get("nearby", [StoreController::class, "NearbyStores"]);
Route::post("order", [ProductController::class, "Order"]);
Route::post("pay", [PaymentController::class, "Pay"]);
Route::post("callback", [PaymentController::class, "CallBack"]);