<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create('payments', function(Blueprint $table) {
			$table->id();
			$table->foreignId("customer_id");
			$table->foreignId("invoice_id")->constrained();
			$table->string("transaction_id",191)->index();
			$table->enum("status", ["pending", "successful", "failed"]);
			$table->bigInteger("amount");
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::dropIfExists('payments');
	}
}
